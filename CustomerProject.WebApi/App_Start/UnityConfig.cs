using Microsoft.Practices.Unity;
using System.Web.Http;
using CustomerProject.Core.Interfaces;
using CustomerProject.Infrastructure.CustomerRepository;
using Unity.WebApi;

namespace CustomerProject.WebApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}