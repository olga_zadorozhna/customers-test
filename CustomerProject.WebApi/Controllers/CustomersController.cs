﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using CustomerProject.BL.Models;
using CustomerProject.BL.Providers;
using CustomerProject.Core.Entities;
using CustomerProject.WebApi.Models;
using Microsoft.Practices.Unity;

namespace CustomerProject.WebApi.Controllers
{

    [RoutePrefix("api/customers")]
    public class CustomersController : BaseController
    {

        [Dependency]
        public CustomerProvider CustomerProvider { get; set; }
        
        // GET: api/Customers
        [Route("")]
        public IEnumerable<Customer> GetCustomers()
        {
            return CustomerProvider.GetAllCustomers();
        }

        // GET: api/Customers/5
        [Route("{id}")]
        [ResponseType(typeof (Customer))]
        public IHttpActionResult GetCustomer(long id)
        {
            var customer = CustomerProvider.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // PUT: api/Customers/5
        [Route("{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCustomer([FromUri]long id, [FromBody]CustomerUpdateModel customerUpdateModel)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                CustomerProvider.UpdateCustomer(id, customerUpdateModel);
            }
            catch (HttpException ex)
            {
                return CreateCustomResponse(ex);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //POST: api/Customers
       [Route("")]
       [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer([FromBody]CustomerUpdateModel customerUpdateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var customer = CustomerProvider.CreateCustomer(customerUpdateModel);
            string location = Request.RequestUri + "/" + customer.CustomerId;
            return Created(location, customer);
        }

        // DELETE: api/Customers/5
        [Route("{id}")]
        public IHttpActionResult DeleteCustomer([FromUri]long id)
        {
            try
            {
                CustomerProvider.DeleteCustomer(id);
            }
            catch (HttpException ex)
            {
                return CreateCustomResponse(ex);
            }    

            return Ok();
        }

        [Route("{id}/orders")]
        [ResponseType(typeof(OrderDto))]
        public IHttpActionResult GetCustomerOrders([FromUri]long id)
        {
            var orders = new List<OrderDto>();
            try
            {
                orders = CustomerProvider.GetCustomerOrders(id);
            }
            catch (HttpException ex)
            {
                return CreateCustomResponse(ex);
            }
            return Ok(orders);
        }



    }
}