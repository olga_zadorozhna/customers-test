﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using CustomerProject.BL.Models;
using CustomerProject.BL.Providers;
using CustomerProject.Core.Entities;
using Microsoft.Practices.Unity;

namespace CustomerProject.WebApi.Controllers
{
    [RoutePrefix("api/orders")]
    public class OrdersController : BaseController
    {
        [Dependency]
        public OrderProvider OrderProvider { get; set; }

        // GET: api/Orders
        [Route("")]
        public IEnumerable<Order> GetOrders()
        {
            return OrderProvider.GetAllOrders();
        }

        // GET: api/Orders/5
        [Route("{id}")]
        [ResponseType(typeof(Order))]
        public IHttpActionResult GetOrder(long id)
        {
            var order = OrderProvider.GetOrderById(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
        [Route("{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOrder(OrderUpdateModel order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                OrderProvider.UpdateOrder(order);
            }
            catch (HttpException ex)
            {
                return CreateCustomResponse(ex);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Orders
        [Route("")]
        [ResponseType(typeof(Order))]
        public IHttpActionResult PostOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            order.OrderDate = DateTime.Now;
            var orderEntity = OrderProvider.CreateOrder(order);
            string location = Request.RequestUri + "/" + orderEntity.CustomerId;
            return Created(location, orderEntity);
        }

        // DELETE: api/Orders/5
        [Route("{id}")]
        [ResponseType(typeof(Order))]
        public IHttpActionResult DeleteOrder(long id)
        {
            try
            {
                OrderProvider.DeleteOrder(id);
            }
            catch (HttpException ex)
            {
                return CreateCustomResponse(ex);
            }

            return Ok();
        }
    }
}