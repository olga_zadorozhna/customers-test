﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace CustomerProject.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        protected IHttpActionResult CreateCustomResponse(HttpException ex)
        {
            return ResponseMessage(Request.CreateErrorResponse((HttpStatusCode)ex.GetHttpCode(), ex.Message));
        }
    }
}
