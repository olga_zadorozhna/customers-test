﻿using System.Web.Http;
using CustomerProject.Infrastructure.DAL;

namespace CustomerProject.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            UnityConfig.RegisterComponents();
            var db = new CustomerDbInitialize();
            System.Data.Entity.Database.SetInitializer(db);
        }
    }
}
