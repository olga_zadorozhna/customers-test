﻿using System;
using System.Collections.Generic;
using CustomerProject.Infrastructure.CustomerRepository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomerProject.WebApi.Controllers;

using CustomerProject.Core.Entities;
using CustomerProject.Core.Interfaces;
using Microsoft.Practices.Unity;
using NUnit.Framework;

namespace CustomerProject.Tests
{
    [TestClass]
    public class CustomerControllerTest
    {
        private IRepository<Customer> customers;
        protected readonly IUnityContainer Container = new UnityContainer();

    [SetUp]
        public void Setup()
        {
            customers = Container.Resolve<IRepository<Customer>>();
        }

        [TestMethod]
        public void GetCustomerOrders()
        {
            var controller = new CustomersController();

        }

        private List<Customer> GetCustomers()
        {
            var testCustomers = new List<Customer>();
            var customer = new Customer()
            {
                CustomerId = 1,
                Title = "Title1",
                FirstName = "FirstName1",
                LastName = "LastName1"s
            };
            var customer2 = new Customer()
            {
                CustomerId = 2,
                Title = "Title2",
                FirstName = "FirstName2",
                LastName = "LastName2"
            };
            testCustomers.Add(customer);
            testCustomers.Add(customer2);
            return testCustomers;
        }
    }
}
