﻿
namespace CustomerProject.BL.Errors
{
    class ApiErrorMessages
    {
        public const string CustomerNotFound = "Customer not found";

        public const string CustomerTitleRequired = "Title is required";

        public const string CustomerFirstNameRequired = "First name is required";

        public const string CustomerLastNameRequired = "Last name is required";

        public const string OrderNotFound = "Order not found";
    }
}
