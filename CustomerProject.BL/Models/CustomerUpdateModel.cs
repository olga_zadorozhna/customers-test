﻿using System.ComponentModel.DataAnnotations;

namespace CustomerProject.BL.Models
{
    public class CustomerUpdateModel
    {
        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }
    }
}
