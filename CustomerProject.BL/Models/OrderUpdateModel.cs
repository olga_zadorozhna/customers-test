﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CustomerProject.Core.Entities;

namespace CustomerProject.BL.Models
{
    public class OrderUpdateModel
    {
        public long OrderId { get; set; }
        
        [Required]
        public DateTime OrderDate { get; set; }
        
        public virtual ICollection<OrderLine> OrderLines { get; set; }
    }
}
