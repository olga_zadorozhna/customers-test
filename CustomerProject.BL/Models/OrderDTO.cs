﻿using System;

namespace CustomerProject.WebApi.Models
{
    public class OrderDto
    {
        public long OrderId { get; set; }
        
        public DateTime OrderDate { get; set; }

        public double TotalPrice { get; set; }
    }
}