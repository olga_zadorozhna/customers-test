﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using CustomerProject.BL.Errors;
using CustomerProject.BL.Models;
using CustomerProject.Core.Entities;
using CustomerProject.Core.Interfaces;
using Microsoft.Practices.Unity;

namespace CustomerProject.BL.Providers
{
    public class OrderProvider
    {
        [Dependency]
        public IRepository<Order> Orders { protected get; set; }

        [Dependency]
        public OrderLineProvider OrderLineProvider { protected get; set; }

        public List<Order> GetAllOrders()
        {
            return Orders.Select().ToList();
        }

        public Order GetOrderById(long id)
        {
            return Orders.Select().FirstOrDefault(c => c.OrderId == id);
        }

        public Order CreateOrder(Order order)
        {
            var orderEntity = new Order()
            {
                CustomerId = order.CustomerId,
                OrderDate = order.OrderDate
            };

            Orders.Add(orderEntity);
            Orders.SaveChanges();
            var id = orderEntity.OrderId;

            OrderLineProvider.AddOrderLines(id, order.OrderLines);

            orderEntity = GetOrderById(id);

            return orderEntity;
            
        }

        public void UpdateOrder(OrderUpdateModel order)
        {
            var orderEntity = CheckIfExistOrder(order.OrderId);

            orderEntity.OrderDate = order.OrderDate;
            orderEntity.OrderLines = order.OrderLines;

            Orders.SaveChanges();
        }

        public void DeleteOrder(long id)
        {
            var orderEntity = CheckIfExistOrder(id);

            Orders.Delete(orderEntity);
            Orders.SaveChanges();
        }

        private Order CheckIfExistOrder(long id)
        {
            var orderEntity = GetOrderById(id);
            if (orderEntity == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, ApiErrorMessages.OrderNotFound);
            }

            return orderEntity;
        }
    }
}
