﻿using System.Net;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using CustomerProject.BL.Models;
using CustomerProject.Core.Interfaces;
using  CustomerProject.Core.Entities;
using Microsoft.Practices.Unity;
using CustomerProject.BL.Errors;
using CustomerProject.WebApi.Models;


namespace CustomerProject.BL.Providers
{
    public class CustomerProvider
    {
        [Dependency]
        public IRepository<Customer> Customers { protected get; set; }
        
        public List<Customer> GetAllCustomers()
        {
            return Customers.Select().ToList();
        }

        public Customer GetCustomerById(long id)
        {
            return Customers.Select().FirstOrDefault(c => c.CustomerId == id);
        }

        public Customer CreateCustomer(CustomerUpdateModel customerUpdateModel)
        {
            var customerEntity = new Customer()
            {
                Title = customerUpdateModel.Title,
                FirstName = customerUpdateModel.FirstName,
                LastName = customerUpdateModel.LastName
            };

            Customers.Add(customerEntity);
            Customers.SaveChanges();

            return customerEntity;

        }

        public void UpdateCustomer(long id, CustomerUpdateModel customerUpdateModel)
        {
            var customerEntity = CheckIfExistCustomer(id);

            customerEntity.Title = customerUpdateModel.Title;
            customerEntity.FirstName = customerUpdateModel.FirstName;
            customerEntity.LastName = customerUpdateModel.LastName;

            Customers.SaveChanges();
        }

        public void DeleteCustomer(long id)
        {
            var customerEntity = CheckIfExistCustomer(id);

            Customers.Delete(customerEntity);
            Customers.SaveChanges();
        }

        public List<OrderDto> GetCustomerOrders(long id)
        {
            var customerEntity = CheckIfExistCustomer(id);
           
            var orderList = customerEntity.Orders;
            if (!orderList.Any())
            {
                return null;
            }

            var orders = new List<OrderDto>();
            foreach (var order in orderList)
            {
                var orderSum = default(double);
                foreach (var orderLine in order.OrderLines)
                {
                    orderSum += orderLine.Qunatity * orderLine.UnitPrice;
                }

                orders.Add(new OrderDto()
                {
                    OrderId = order.OrderId,
                    OrderDate = order.OrderDate,
                    TotalPrice = orderSum
                });
            }

            return orders;
        }

        private Customer CheckIfExistCustomer(long id)
        {
            var customerEntity = GetCustomerById(id);
            if (customerEntity == null)
            {
                throw new HttpException((int)HttpStatusCode.NotFound, ApiErrorMessages.CustomerNotFound);
            }

            return customerEntity;
        }
        

       
    }
}
