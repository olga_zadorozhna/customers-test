﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using CustomerProject.Core.Entities;
using CustomerProject.Core.Interfaces;

namespace CustomerProject.BL.Providers
{
    public class OrderLineProvider
    {
        [Dependency]
        public IRepository<OrderLine> OrderLines { protected get; set; }

        public OrderLine GetOrderLineById(long orderLineId)
        {
            return OrderLines.Select().FirstOrDefault(c => c.OrderLineId == orderLineId);
        }

        public IEnumerable<OrderLine> GetOrderLinesByOrderId(long orderId)
        {
            return OrderLines.Select().Where(c => c.OrderId == orderId);
        }

        public void AddOrderLines(long orderId, IEnumerable<OrderLine> orderLines )
        {
            foreach (var orderLine in orderLines)
            {
                orderLine.OrderId = orderId;
                OrderLines.Add(orderLine);
            }
            OrderLines.SaveChanges();
        }
    }
}
