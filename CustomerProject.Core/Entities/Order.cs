﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerProject.Core.Entities
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        public long OrderId { get; set; }

        [Required]
        public long CustomerId { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        public virtual ICollection<OrderLine> OrderLines { get; set; }

    }
}
