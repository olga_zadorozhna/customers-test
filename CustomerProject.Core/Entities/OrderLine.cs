﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerProject.Core.Entities
{
    [Table("OrderLines")]
    public class OrderLine
    {
        [Key]
        public long OrderLineId { get; set; }

        public long OrderId { get; set; }

        [Required]
        public string SKU { get; set; }

        [Required]
        public int Qunatity { get; set; }

        [Required]
        public double UnitPrice { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

    }
}
