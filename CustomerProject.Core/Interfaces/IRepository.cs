﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace CustomerProject.Core.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Select();

        IQueryable<TEntity> Select<TProperty>(Expression<Func<TEntity, TProperty>> path);

        TEntity Add(TEntity entity);

        TEntity Update(TEntity entity);
        
        void Delete(TEntity entity);

        void SaveChanges();
    }
}
