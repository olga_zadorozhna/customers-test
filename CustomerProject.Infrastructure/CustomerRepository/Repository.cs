﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CustomerProject.Core.Interfaces;
using CustomerProject.Infrastructure.DAL;

namespace CustomerProject.Infrastructure.CustomerRepository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity: class
    {
        private readonly CustomerContext context;

        private readonly IDbSet<TEntity> dbSet;

        public Repository(CustomerContext context)
        {
            this.context = context;
            dbSet = this.context.Set<TEntity>();
        }

        private IDbSet<TEntity> DbSet
        {
            get
            {
                return dbSet;
            }
        }

        public IQueryable<TEntity> Select()
        {
            return DbSet;
        }

        public IQueryable<TEntity> Select<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            return Select().Include(path);
        }

        public TEntity Add(TEntity entity)
        {
            return DbSet.Add(entity);
        }

        public TEntity Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
