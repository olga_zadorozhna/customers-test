﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using CustomerProject.Core.Entities;

namespace CustomerProject.Infrastructure.DAL
{
    public class CustomerDbInitialize : DropCreateDatabaseIfModelChanges<CustomerContext>
    {

        protected override void Seed(CustomerContext context)
        {
            
                var customer = new Customer()
                {
                    CustomerId = 1,
                    Title = "Title1",
                    FirstName = "FirstName1",
                    LastName = "LastName1"
                };
                context.Customers.Add(customer);
            var customer2 = new Customer()
            {
                CustomerId = 2,
                Title = "Title2",
                FirstName = "FirstName2",
                LastName = "LastName2"
            };
            context.Customers.Add(customer2);
            context.SaveChanges();


            var orders = new List<Order>();

                    var order1 = new Order
                    {
                        CustomerId = 1,
                        OrderDate = DateTime.UtcNow.AddDays(-1)
                    };

            var order2 = new Order
            {
                CustomerId = 2,
                OrderDate = DateTime.UtcNow.AddDays(-2)
            };


            var order3 = new Order
            {
                CustomerId = 2,
                OrderDate = DateTime.UtcNow.AddDays(-3)
            };
            context.Orders.Add(order1);
            context.SaveChanges();
            context.Orders.Add(order2);
            context.SaveChanges();
            context.Orders.Add(order3);

            context.SaveChanges();

            

               
                    var orderLine1 = new OrderLine
                    {
                        OrderId = 1,
                        SKU = "Bangalore1",
                        Qunatity = 1,
                        UnitPrice = 10
                    };
            var orderLine2 = new OrderLine
            {
                OrderId = 1,
                SKU = "Bangalore2",
                Qunatity = 2,
                UnitPrice = 10
            };
            var orderLine3 = new OrderLine
            {
                OrderId = 2,
                SKU = "Bangalore3",
                Qunatity = 3,
                UnitPrice = 10
            };
            var orderLine4 = new OrderLine
            {
                OrderId = 3,
                SKU = "Bangalore4",
                Qunatity = 4,
                UnitPrice = 10
            };

            context.OrderLines.Add(orderLine1);
            context.OrderLines.Add(orderLine2);
            context.OrderLines.Add(orderLine3);

            context.OrderLines.Add(orderLine4);
                
           

            context.SaveChanges();

            base.Seed(context);

        }
    }
}
