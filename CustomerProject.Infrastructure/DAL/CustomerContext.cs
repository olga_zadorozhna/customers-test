﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using CustomerProject.Core.Entities;

namespace CustomerProject.Infrastructure.DAL
{
    public class CustomerContext : DbContext
    {
        public CustomerContext() : base("name=CustomerContext")
        {
        }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderLine> OrderLines { get; set; }
    }
}
